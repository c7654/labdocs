+++
layout="docs"
title= "Code"
description = ""
group= "jeux"
section="apprentissage"
toc= true
date= "2022-10-25T10:47:33+02:00"
draft= false
+++
{{< callout >}}
### Source
[15 jeux pour apprendre à coder !](https://www.youtube.com/watch?v=eDrjSMK_cyU)
{{< /callout >}}
[CodinGame](https://www.codingame.com/)  
[CodeWars](https://www.codewars.com/)  

{{< a-test >}}
The current link item
{{< /a-test >}}
