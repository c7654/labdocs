+++
layout="docs"
title= "Divers"
description = ""
group= "jeux"
section="apprentissage"
toc= true
date= "2022-10-25T10:48:37+02:00"
draft= false
+++
{{< callout >}}
### Source
[15 jeux pour apprendre à coder !](https://www.youtube.com/watch?v=eDrjSMK_cyU)
{{< /callout >}}
[Git](https://learngitbranching.js.org/)  
[RegexOne](https://regexone.com/lesson/introduction_abcs)  
[RegexLearn](https://regexlearn.com/)  