+++
layout="docs"
title= "Installation"
description = "INSTALLER HUGO SUR VOTRE POSTE DE TRAVAIL"
group= "hugo"
section="commencer"
toc= true
date= "2023-05-15T11:25:16+02:00"
draft= false
+++
Il est préférable d'installer `Hugo` sur Linux, bien que la version soit aussi disponible pour Windows.  
Pour windows via [wsl](https://lecrabeinfo.net/installer-wsl-windows-subsystem-for-linux-sur-windows-10.html), vous pouvez installer une machine Linux, le plus simple étant la distribution UBUNTU.

## Installation de GO
Go est un langage de programmation compilé et concurrent inspiré de C et Pascal. Il a été développé par Google 5 à partir 
d’un concept initial de Robert Griesemer (en), Rob Pike et Ken Thompson.

### Téléchargement de GO
La version se trouve sur ce [site](https://go.dev/dl/)
Pour UBUNTU voici les instructions :

Si vous devez faire une mise à jour, il faut supprimer la version existante

```bash
sudo rm -rf /usr/local/go
```
L'installation se fait alors pour la version x.x.x :
```bash
sudo wget -c https://dl.google.com/go/gox.x.x.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local
```
Il faut l'initialiser dans le profile
```bash
nano ~/.profile
```
et insérer à la fin de la page :
```bash
export PATH=$PATH:/usr/local/go/bin
```
il faut activer le profile
```bash
source ~/.profile
```
et vérifier qu'il est bien installé
```bash
go version
```

## Installer HUGO
La version se trouve sur ce [site](https://github.com/gohugoio/hugo/releases)
{{< callout>}}
Il faut choisir la version **extended**
{{< /callout>}}
Si vous devez faire une mise à jour, il faut supprimer la version existante
```bash
sudo rm -rf /usr/local/bin/hugo
```
L'installation se fait alors pour la version x.x.x :
```bash
sudo wget https://github.com/gohugoio/hugo/releases/download/vx.x.x/hugo_extended_x.x.x_Linux-64bit.tar.gz -O - | sudo tar -xz -C /usr/local/bin
```



