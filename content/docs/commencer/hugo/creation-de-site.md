+++
layout="docs"
title= "Creation De Site"
description = "Créer des sites statiques avec HUGO"
group= "hugo"
section="commencer"
toc= true
date= "2023-05-15T11:25:16+02:00"
draft= false
+++
## Présentation

{{< callout>}}
Voici un lien qui vous explique ce qu'est un site statique :  
[Qu'est-ce que JAMStack et un site statique](https://www.youtube.com/watch?v=ytIwbEqtFpM&t=550s)  
Voici 2 liens qui vous expliquent comment créer et mettre en ligne un site statique :  
[Tour d'horizon pour un premier site statique](https://www.youtube.com/watch?v=AmajDfWLIsg)  
[Mise en ligne sur les pages Gitlab](https://www.youtube.com/watch?v=wnV2mWuSZYA)  

{{< /callout>}}

Le lien du site `HUGO` est le [suivant](https://gohugo.io/)

## Créer un site à partir d'un thème
Choisir un modèle à partir de ce [lien](https://themes.gohugo.io/)  
L'installation est expliquée dans la documentation.

## Créer un site à blanc
Il existe plusieurs tutos, celui-ci me parait [intéressant](https://retrolog.io/blog/creating-a-hugo-theme-from-scratch/).
### Création du site
Créer un nouveau site monsite :
```bash
hugo new site monsite
```
Aller dans le répertoire :
```bash
cd monsite
```
Créer un nouveau thème :
```bash
hugo new theme monsiteTheme
```
Créer une page :
```bash
Hugo new <chemin>/mapage.md
```

### Tester le site avec son modèle
Rajouter le thème dans `config.tolm`
```bash
theme=" exampleTheme "
```
Dans le layout rajouter
```bash
Index.html
```

### Mettre en ligne le site
{{< callout >}}
Vérifier le lien dans `baseURL` de `config.tolm`
{{< /callout >}}
Création des pages html
```bash
hugo
```
Les pages sont dans le répertoire `public`. Il suffit de les uploader dans le serveur.

### Utiliser hugo
Lancer hugo en local :
```bash
hugo server
```
Créer une page markdown :
```bash
Hugo new <chemin>/mapage.md
```